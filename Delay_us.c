/*! \file  Delay_us.c
 *
 *  \brief Delay for a short time
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 6:53 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! Delay_us - short delay */

/*! Function loops for as many times as requested by the caller
 *
 * \param len int Number of times to loop
 * \returns none
 */
void Delay_us(unsigned int len)
{
  int i;
  for ( i=0; i<len; i++ )
    ;

}
