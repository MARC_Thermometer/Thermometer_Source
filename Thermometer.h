/*! \file  Thermometer.h
 *
 *  \brief Definitions for Thermometer
 *
 *  This file contains global definitions for the thermometer, as well
 *  as function prototypes for functions specific to the application.
 *  Prototypes for library functions (LCD and DS1821) are included in
 *  their appropriate headers.
 *
 *
 *  \author jjmcd
 *  \date December 27, 2014, 4:07 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef KM202_TEST1821_H
#define	KM202_TEST1821_H

#ifdef	__cplusplus
extern "C"
{
#endif


/*! Define the data direction bit for the LED */
#define LEDTRIS _TRISB6
/*! Define the output for the LED */
#define LED _LATB6
/*! Define the illuminated state for the LED */
#define ON 1
/*! Define the dark state for the LED */
#define OFF 0

/*! Initialization for Thermometer */
void Initialize( void );

/*! Display the temperature in F and C as well as min and max (in F) */
void displayResult( void );

/*! Get the temperature from the DS1821 */
void getTemp( void );

/*! Perform the calculations for the thermometer  */
void doCalculations(void);

/*! Delay for a specified number of microseconds */
void Delay_us( unsigned int );


#ifdef	__cplusplus
}
#endif

#endif	/* KM202_TEST1821_H */

