#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=Thermometer.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=thermometer.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/thermometer.x.tar
# Blue_3 configuration
CND_ARTIFACT_DIR_Blue_3=dist/Blue_3/production
CND_ARTIFACT_NAME_Blue_3=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Blue_3=dist/Blue_3/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Blue_3=${CND_DISTDIR}/Blue_3/package
CND_PACKAGE_NAME_Blue_3=thermometer.x.tar
CND_PACKAGE_PATH_Blue_3=${CND_DISTDIR}/Blue_3/package/thermometer.x.tar
# Red_1 configuration
CND_ARTIFACT_DIR_Red_1=dist/Red_1/production
CND_ARTIFACT_NAME_Red_1=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Red_1=dist/Red_1/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Red_1=${CND_DISTDIR}/Red_1/package
CND_PACKAGE_NAME_Red_1=thermometer.x.tar
CND_PACKAGE_PATH_Red_1=${CND_DISTDIR}/Red_1/package/thermometer.x.tar
# Simulate configuration
CND_ARTIFACT_DIR_Simulate=dist/Simulate/production
CND_ARTIFACT_NAME_Simulate=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Simulate=dist/Simulate/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Simulate=${CND_DISTDIR}/Simulate/package
CND_PACKAGE_NAME_Simulate=thermometer.x.tar
CND_PACKAGE_PATH_Simulate=${CND_DISTDIR}/Simulate/package/thermometer.x.tar
# Blue_4 configuration
CND_ARTIFACT_DIR_Blue_4=dist/Blue_4/production
CND_ARTIFACT_NAME_Blue_4=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Blue_4=dist/Blue_4/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Blue_4=${CND_DISTDIR}/Blue_4/package
CND_PACKAGE_NAME_Blue_4=thermometer.x.tar
CND_PACKAGE_PATH_Blue_4=${CND_DISTDIR}/Blue_4/package/thermometer.x.tar
# Red_2 configuration
CND_ARTIFACT_DIR_Red_2=dist/Red_2/production
CND_ARTIFACT_NAME_Red_2=Thermometer.X.production.hex
CND_ARTIFACT_PATH_Red_2=dist/Red_2/production/Thermometer.X.production.hex
CND_PACKAGE_DIR_Red_2=${CND_DISTDIR}/Red_2/package
CND_PACKAGE_NAME_Red_2=thermometer.x.tar
CND_PACKAGE_PATH_Red_2=${CND_DISTDIR}/Red_2/package/thermometer.x.tar
