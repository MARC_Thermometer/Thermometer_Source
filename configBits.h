/*! \file  configBits.h
 *
 *  \brief Configuration fuses for the PIC24FV16KM202
 *
 *  This file, which is only referenced in the mainline, sets the
 *  configuration bits for the PIC.  These bits determine such things
 *  as what oscillator to use, whether oscillator switching is permitted,
 *  and whether to enable the watchdog timer and brownout detection.
 *
 *  In this application, the watchdog timer is used, and the primary
 *  internal RC oscillator is selected with the PLL and postscaler.
 *
 *  The bits are set using provided pragmas.
 *
 *  Since the application manipulates the clocks, some discussion of the
 *  available clocks is in order.  The primary clock can be driven by a crystal
 *  or an onboard 8MHz RC clock.  In this application, the internal
 *  RC clock has been selected as timing is not critical.  The primary clock
 *  may optionally be followed by a PLL which multiplies the clock by 4.
 *  Since the PIC executes one instruction every 2 clocks, that results
 *  in 16 million instructions per second (16 MIPS).  The primary clock
 *  may optionally be followed by a postscaler, allowing many options for
 *  slower clocks.  In this application, we choose 1 MIPS and 3.9 KIPS.
 *
 *  In addition to the primary clock, there is a 32 kHz secondary clock
 *  which may be used as the processor clock, and/or the input to a watchdog
 *  timer.  The watchdog timer has a prescaler and a postcaler, allowing
 *  for watchdog timeouts between 1 millisecond and 4 minutes.  In this
 *  application, we will use a watchdog timeout of approximately one minute.
 *
 *  \author jjmcd
 *  \date December 28, 2014, 2:52 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CONFIGBITS_H
#define	CONFIGBITS_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! Primary oscillator selection */
#pragma config FNOSC = FRCPLL   // Oscillator Select (Fast RC Oscillator with
                                // Postscaler and PLL Module (FRCDIV+PLL))
/*! Low power oscillator selection */
#pragma config LPRCSEL = LP     // LPRC Oscillator Power and
                                // Accuracy (Low Power, Low Accuracy Mode)
/*! Internal/External Oscillator Switching */
#pragma config IESO = OFF       // Internal External Switch Over bit (Internal
                                // External Switchover mode disabled (Two-speed
                                // Start-up disabled))
/*! Clock hinting */
#pragma config POSCFREQ = MS    // Primary Oscillator Frequency Range 
                                // Configuration bits (Primary oscillator/external
                                // clock input frequency between 100kHz and 8MHz)
/*! Secondary oscillator */
#pragma config SOSCSEL = SOSCLP // SOSC Power Selection Configuration bits 
                                //(Secondary Oscillator configured for
                                // low-power operation)
/*! Watchdog timer postscaler */
#pragma config WDTPS = PS8192   // Watchdog Timer Postscale Select bits (1:8192)
/*! Watchdog timer prescaler */
#pragma config FWPSA = PR128    // WDT Prescaler bit (WDT prescaler ratio of 1:128)
/*! Watchdog timer on */
#pragma config FWDTEN = ON      // Watchdog Timer Enable bits (WDT enabled in hardware)


/*  ---  the following bits are left at their defaults, but probably    ---
 *  ---  will be changed to the values shown for many projects          --- */

///*! CLK0 pin or I/O */
//#pragma config OSCIOFNC = IO    // CLKO Enable Configuration bit (Port I/O enabled (CLKO disabled))
//
///*! CLK digital */
//#pragma config SOSCSRC = DIG        // SOSC Source Type (Analog Mode for use with crystal)


/*  ---  the following bits are left at their defaults, included here   ---
 *  ---  only to provide documentation                                  --- */

// FBS
#pragma config BWRP = OFF           // Boot Segment Write Protect (Disabled)
#pragma config BSS = OFF            // Boot segment Protect (No boot program flash segment)

// FGS
#pragma config GWRP = OFF           // General Segment Write Protect (General segment may be written)
#pragma config GCP = OFF            // General Segment Code Protect (No Protection)

// FOSC
#pragma config POSCMOD = NONE       // Primary Oscillator Configuration bits (Primary oscillator disabled)
#pragma config FCKSM = CSDCMD       // Clock Switching and Monitor Selection (Both Clock Switching and Fail-safe Clock Monitor are disabled)

// FWDT
#pragma config WINDIS = OFF         // Windowed Watchdog Timer Disable bit (Standard WDT selected(windowed WDT disabled))

// FPOR
#pragma config BOREN = BOR3         // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware, SBOREN bit disabled)
#pragma config RETCFG = OFF         //  (Retention regulator is not available)
#pragma config PWRTEN = ON          // Power-up Timer Enable bit (PWRT enabled)
#pragma config I2C1SEL = PRI        // Alternate I2C1 Pin Mapping bit (Use Default SCL1/SDA1 Pins For I2C1)
#pragma config BORV = V18           // Brown-out Reset Voltage bits (Brown-out Reset set to lowest voltage (1.8V))
#pragma config MCLRE = ON           // MCLR Pin Enable bit (RA5 input pin disabled, MCLR pin enabled)

// FICD
#pragma config ICS = PGx1           // ICD Pin Placement Select bits (EMUC/EMUD share PGC1/PGD1)

  
#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGBITS_H */

