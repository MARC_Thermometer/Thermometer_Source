/*! \file  Thermometer.c
 *
 *  \brief Mainline to read and display DS1821 temperature sensor
 *
 *
 *  \author jjmcd
 *  \date November 20, 2014, 8:10 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include "Thermometer.h"
#include "../libLCD.X/LCD.h"
#include "../libDS1821.X/DS1821.h"
#include "configBits.h"

/*! Temperature in Centigrade read from Dallas DS1821 */
char TempC;
/*! Temperature in Farenheit */
int TempF;
/*! Lowest temperature encountered */
int TempLow;
/*! Highest temperature encountered */
int TempHigh;

/*! main - Mainline for test DS1821*/
/*! main()
 *
 * Initialize the DS1821 temperature and the LCD display.  Then loop,
 * reading the DS1821, formatting the result, and displaying it.  The
 * function then enters Sleep(), from which it is awakened by the
 * watchdog timer.  When the timer expires the loop is executed once
 * again.
 *
 * Initialization, reading the temperature, calculations and display are
 * each handled by their own function.  The library Sleep() function causes
 * the processor to stop.  The primary oscillator is stopped, but the
 * secondary, low power oscillator remains running as it controls the
 * watchdog timer.  When the watchdog timer expires, (approximately
 * one minute) the program resumes execution.
 *
 * \anchor main_diagram
 * \dot
 * digraph main
 * {
 *   fontname="Helvetica-Bold";
 *   fontcolor="#602000";
 *   label="Main program logic flow";
 *   node [ color="burlywood" fontcolor="#602000"  fillcolor="cornsilk"
 *     fontname="Helvetica" style="filled" ];
 *   edge [ color="sienna" ];
 *   enter->initialize;
 *   initialize->"get temperature";
 *   "get temperature"->"perform calculations";
 *   "perform calculations"->"display results";
 *   "display results"->sleep;
 *   sleep->"get temperature";
 * }
 * \enddot
 *
 * \callgraph
 *
 */
int main( void )
{
  Initialize();

  // Keep doing this for a very long time
  while(1)
    {
      // Get the temperature
      getTemp();
      // Perform calculations
      doCalculations();
      // Display the results
      displayResult();
      // Wait a while until the next reading
      Sleep();
    }
  return 0;
}
