/*! \file  displayResult.c
 *
 *  \brief Display temperatures on LCD
 *
 *  The displayResult() function displays the Centigrade and Farenheit
 *  temperatures on the LCD, along with the minimum and maximum
 *  (in Farenheit).
 *
 *  \author jjmcd
 *  \date December 28, 2014, 4:00 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <stdio.h>
#include "../libLCD.X/LCD.h"

/*! Temperature in Centigrade read from Dallas DS1821 */
extern char TempC;
/*! Temperature in Farenheit */
extern int TempF;
/*! Lowest temperature encountered */
extern int TempLow;
/*! Highest temperature encountered */
extern int TempHigh;

/*! Display the temperature in F and C as well as min and max (in F) */
/*! displayResult() creates two strings, one for the upper and one for the
 *  lower line of the LCD display.  The upper line shows the temperature
 *  in Farenheit and Centigrade while the lower line shows the minimum
 *  and maximum temperatures encountered.
 *
 *  Both strings are 16 characters long, the full width of the LCD, so that
 *  the LCD need not be cleared each update.  The two strings are then
 *  displayed on the appropriate line of the LCD.
 * \dot
 * digraph displayResult
 * {
 *   fontname="Helvetica-Bold";
 *   fontcolor="#602000";
 *   label="displayResult() logic flow";
 *   node [ color="burlywood" fontcolor="#602000"  fillcolor="cornsilk"
 *     fontname="Helvetica" style="filled" ];
 *   edge [ color="sienna" ];
 *   enter->"Format results";
 *   "Format results"->"Home cursor";
 *   "Home cursor"->"Write first line";
 *   "Write first line"->"Position second line";
 *   "Position second line"->"Write second line";
 *   "Write second line"->exit;
 * }
 * \enddot
 *
 */
void displayResult(void)
{
    char szWork1[32],szWork2[32];

    // Format the display strings
    sprintf(szWork1,"  %3d\337C %3d\337F    ",TempC,TempF);
    sprintf(szWork2,"%2d Lo      Hi%3d  ",TempLow,TempHigh);
    // Position cursor at home, will rewrite entire screen
    LCDposition(0x0);
    // Show the current temperature in C and F
    LCDputs(szWork1);
    // Second line of the LCD
    LCDposition(0x40);
    // Display the high and low temperatures
    LCDputs(szWork2);
}
