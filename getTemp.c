/*! \file  getTemp.c
 *
 *  \brief Get the DS1821 temperature
 *
 *  Reads the centigrade temperature from the DS1821.  Sets the clock
 *  speed up during the conversion because more precision in delays is
 *  required.  Also lights the LED to indicate conversion in progress.
 *
 *  \author jjmcd
 *  \date December 28, 2014, 8:39 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include "Thermometer.h"
#include "../libDS1821.X/DS1821.h"

/*! Temperature in Centigrade read from Dallas DS1821 */
extern char TempC;

/*! Get the temperature from the DS1821 */
/*! getTemp() turns on the LED, sets the fast clock, calls the DS1821
 *  library to fetch the temperature, then slows the clock and turns off
 *  the LED.
 *
 * \dot
 * digraph getTemp
 * {
 *   fontname="Helvetica-Bold";
 *   fontcolor="#602000";
 *   label="getTemp() logic flow";
 *   node [ color="burlywood" fontcolor="#602000"  fillcolor="cornsilk"
 *     fontname="Helvetica" style="filled" ];
 *   edge [ color="sienna" ];
 *   enter->"Turn on LED";
 *   "Turn on LED"->"Read DS1821";
 *   "Read DS1821"->"Turn off LED";
 *   "Turn off LED"->exit;
 * }
 * \enddot
 */
void getTemp(void)
{
      // Turn on LED while temperature conversion in process
      LED = ON;
      // Read the temperature
      TempC = DS1821_ReadTemp(1);
      // Turn off the LED to indicate conversion complete
      LED = OFF;

}
