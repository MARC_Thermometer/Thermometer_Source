/*! \file 02readme.txt
 *
 *  \brief Possible future modifications
 *
 *  \page 2mods Possible future modifications
 *
 *  The student may choose to make a number of modifications to the
 *  software and the circuit.  This section outlines a few ideas.
 *
 *  \section backlight Backlight
 *
 *  The provided LCD includes an LED backlight.  Because the backlight
 *  requires significant power, and the project is powered by batteries,
 *  the original project did not power the backlight.  The student may
 *  consider options such as using the button to illuminate the backlight
 *  only when needed, or powering the project from a more robust source.
 *
 *  \section reset Reset min and max temperatures
 *
 *  The minimum and maximum temperatures are remembered from the time the
 *  thermometer is turned on.  The builder may wish to use the pushbutton
 *  to reset the minimum and maximum temperatures, or may wish to have them
 *  reset automatically at some time period.
 *
 *  The pushbutton is a little problematic in that the processor is almost
 *  always sleeping and not checking inputs.  Having to hold the button down
 *  for an entire minute would be annoying at best.  There are other strategies,
 *  such as using the RB0 wake up interrupt, adding a latch to remember that
 *  the button was pressed, etc.  These are left for the builder to explore.
 *
 *  \section power Power options
 *
 *  The project is powered by AA batteries which should provide about three
 *  months of battery life.  Changing to D cells would increase that life
 *  considerably.  Smartphone chargers provide an excellent power source for
 *  5 volt projects, and are available very cheaply from sources such as
 *  Amazon, or perhaps even the local dollar store.  With the use of a
 *  charger the power consumed by the backlight would not be a problem.
 *
 *  The LED is illuminated while the temperature is being sensed.  Although
 *  the LED consumes considerable power compared to the processor, it is only
 *  lit briefly.  Removing the LED code or simply disconnecting the LED would
 *  result in a slight improvement in power consumption.
 *
 *
 */
