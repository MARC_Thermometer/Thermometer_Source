/*! \file  Initialize.c
 *
 *  \brief Initilization for Thermometer
 *
 *
 *  \author jjmcd
 *  \date December 28, 2014, 3:45 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include "../libLCD.X/LCD.h"
#include "../libDS1821.X/DS1821.h"
#include "Thermometer.h"

/*! Lowest temperature encountered */
extern int TempLow;
/*! Highest temperature encountered */
extern int TempHigh;

/*! Initialization for Thermometer */
/*! this function provides the initialization of the various components
 *  used in the thermometer.
 *
 *  The peripherals are all turned off to eliminate the power they might
 *  consume, and the one peripheral that is needed, the timer, is turned
 *  on.
 *
 *  Next, the two ports that are/might be inputs have their corresponding
 *  analog inputs turned off.  If the analog input was left on, these
 *  ports could not be read as digital.
 *
 *  Now the LCD and DS1821 initialization reoutines are called.  Although
 *  we initialized the DS1821 port, the software uses some timers which
 *  are initialized by DS1821_Initialize().
 *
 *  Finally, the low and high temperatures are set to unreasonable values
 *  so that the first time doCalcuations() is run they will be set to the
 *  first temperature read.
 * 
 * \dot
 * digraph Initialize
 * {
 *   fontname="Helvetica-Bold";
 *   fontcolor="#602000";
 *   label="Initialize() logic flow";
 *   node [ color="burlywood" fontcolor="#602000"  fillcolor="cornsilk"
 *     fontname="Helvetica" style="filled" ];
 *   edge [ color="sienna" ];
 *   enter->"Set LED";
 *   "Set LED"->"Turn off unused peripherals"
 *   "Turn off unused peripherals"->"Turn off analogs";
 *   "Turn off analogs"->"Initialize LCD";
 *   "Initialize LCD"->"Initialize DS1821";
 *   "Initialize DS1821"->"Set min/max";
 *   "Set min/max"->"exit";
 * }
 * \enddot
 *
 */
void Initialize(void)
{

  // Initialize the LCD
  LCDinit();
  LCDputs("Initializing");

  // Make the LED port be an output
  LEDTRIS = 0;
  // Turn off the LED
  LED = OFF;

  // Disable all peripherals
  PMD1=PMD2=PMD3=PMD4=PMD6=PMD8=0xffff;
  // Enable the timer
  PMD1bits.T1MD=0;

  // Set the DS1821 input port to digital
  ANSBbits.ANSB7 = 0;   // Make DS1821 port digital

  // Initialize the timers for the DS1821
  DS1821_Initialize(1,&PORTB,7);

  // Initialize the high and low temperatures
  TempLow  = 999;
  TempHigh = -999;

  LCDclear();
}
