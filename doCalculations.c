/*! \file  doCalculations.c
 *
 *  \brief Convert temps, calc min and max
 *
 *  The doCalculations() function converts the Centigrade temperature
 *  read from the DS1821 to Farenheit.  That temperature is then compared
 *  to the highest and lowest temperatures observed and the high and
 *  low potentially updated accordingly.
 *
 *  \author jjmcd
 *  \date December 28, 2014, 9:10 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! Temperature in Centigrade read from Dallas DS1821 */
extern char TempC;
/*! Temperature in Farenheit */
extern int TempF;
/*! Lowest temperature encountered */
extern int TempLow;
/*! Highest temperature encountered */
extern int TempHigh;


/*! Perform the calculations for the thermometer  */
/*! The temperature from the DS1821 is converted to Farenheit.  It is
 *  then compared to the current minimum and maximum temperature, which
 *  are adjusted appropriately.
 * \dot
 * digraph doCalculations
 * {
 *   fontname="Helvetica-Bold";  
 *   fontcolor="#602000";
 *   label="doCalculations() logic flow";
 *   node [ color="burlywood" fontcolor="#602000"  fillcolor="cornsilk"
 *     fontname="Helvetica" style="filled" ];
 *   edge [ color="sienna" fontname="Helvetica" fontcolor="peru" ];
 *   enter->"Convert to F";
 *   "Convert to F"->"is Temp>max?";
 *   "is Temp>max?"->"max=Temp" [label=yes];
 *   "is Temp>max?"->"is Temp<min?" [label=no];
 *   "max=Temp"->"is Temp<min?";
 *   "is Temp<min?"->"min=Temp" [label=yes];
 *   "min=Temp"->exit;
 *   "is Temp<min?"->exit [label=no];
 * }
 * \enddot
 *
 */
void doCalculations(void)
{
      // Convert to farenheit
      TempF = (9*TempC)/5+32;
      // Calculate the new high temperature
      if ( TempF > TempHigh )
        TempHigh = TempF;
      // Calculate the new low temperature
      if ( TempF < TempLow )
        TempLow = TempF;

}
